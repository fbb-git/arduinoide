#define XERR
#include "headers.ih"

void Headers::standard(string const &srcPath) const
{
    softLinks(srcPath + "/*.h", 
              d_options.includePath(), d_options.verbose());
}
