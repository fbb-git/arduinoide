//#define XERR
#include "headers.ih"

Headers::Headers(Options const  &options, Specifications const &specs)
:
    d_specs(specs),
    d_options(options)
{}
