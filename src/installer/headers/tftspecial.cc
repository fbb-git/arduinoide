//#define XERR
#include "headers.ih"

void Headers::tftSpecial(string const &srcPath) const
{
    auto &&[utilityPath, utilClassName, utilClassPath] = 
                                                utilityNames(srcPath);

    TFTChangeBuf tftChangeBuf{ "utility/", utilClassName + '/' };

    copyEdit(srcPath + "/*.h", d_options.includePath(), tftChangeBuf);
    copyEdit(utilityPath + "/*.h", utilClassPath, tftChangeBuf);
}
