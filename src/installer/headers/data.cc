//#define XERR
#include "headers.ih"

void (Headers::*Headers::s_install[])(string const &path) const = 
{
    &Headers::standard,                     // STD
    &Headers::standard,                     // UTILITY2DEST
    &Headers::util,                         // UTIL
    &Headers::util,                         // UTILITYCOMPILE
    &Headers::tftSpecial,                   // TFTSPECIAL
};
