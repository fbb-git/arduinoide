//#define XERR
#include "headers.ih"

tuple<string, string, string> Headers::utilityNames(
                                            string const &srcPath) const
{
                                        // the arduino /library class name
    string className = srcPath.substr(srcPath.rfind('/') + 1);

    string utilClassName = "util" + className;
     
                                            // the util'Class' include path
    string utilClassPath = d_options.includePath() + '/' + utilClassName;
    mkdir(utilClassPath);                 // create, e.g., arduinoide/utilSD

    return { srcPath + "/utility", utilClassName, utilClassPath };
}
