//#define XERR
#include "headers.ih"

void Headers::copyEdit(string const &srcPathPattern, string const &destPath,
                        ChangeBuf &changeBuf) const
{
    for (string src: Glob{ Glob::REGULAR_FILE, srcPathPattern })
    {

        if (d_options.verbose())
            cout << "copy " << src << ", replacing " << changeBuf.search() << 
                                " by " << changeBuf.replace() << '\n';

        string dest{ destPath + src.substr(src.rfind('/')) };

        changeBuf.useIstream(src);

        Exception::factory<ofstream>(dest) << &changeBuf;
    }
}
