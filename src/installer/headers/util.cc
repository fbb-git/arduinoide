#define XERR
#include "headers.ih"

void Headers::util(string const &srcPath) const
{
    auto &&[utilityPath, utilClassName, utilClassPath] = 
                                                utilityNames(srcPath);

                                        // link the .../utility/*.h files 
    softLinks(utilityPath + "/*.h", utilClassPath, d_options.verbose());


    ChangeBuf changeBuf{ "utility/", utilClassName + '/' };
    copyEdit(srcPath + "/*.h", d_options.includePath(), changeBuf);
}
