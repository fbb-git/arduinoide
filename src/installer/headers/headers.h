#ifndef INCLUDED_HEADERS_
#define INCLUDED_HEADERS_

#include <iosfwd>
#include "../fs/fs.h"

class Specifications;
class Options;
class ChangeBuf;
    
class Headers: private FS
{
    Specifications const &d_specs;
    Options const &d_options;

    static void (Headers::*s_install[])(std::string const &path) const;

    public:
        Headers(Options const  &options, Specifications const &specs);

        void install();

    private:
        void standard(std::string const &path) const;
        void standardCopy(std::string const &path) const;
        void util(std::string const &path) const;
        void tftSpecial(std::string const &path) const;

        void copyEdit(std::string const &srcPath, 
                    std::string const &includePath, 
                    ChangeBuf &changeBuf) const;

        std::tuple<std::string, std::string, std::string> utilityNames(
                                    std::string const &srcPath) const;
};
        
#endif
