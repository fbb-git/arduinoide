#define XERR
#include "headers.ih"

void Headers::install()
{
    for (size_t idx = 0, end = d_specs.size(); idx != end; ++idx)
    {
        auto const &spec = d_specs[idx];

        cout << "installing headers from " << spec.path << " in " <<
                d_options.includePath() << '\n';

        ( this->*s_install[spec.type] )(spec.path); // STD -> standard
    }
}
