//#define XERR
#include "changebuf.ih"

void ChangeBuf::useIstream(string const &src)
{
    d_inName = src;
    d_in = Exception::factory<ifstream>(src);
}
