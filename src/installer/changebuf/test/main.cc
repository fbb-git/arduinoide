//#define XERR
#include "main.ih"

int main(int argc, char **argv)
try
{
    auto in{Exception::factory<ifstream>("main.cc")};

    ChangeBuf buf{ in,
                    "#include", "#incFile" };

    cout << &buf;
    
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
