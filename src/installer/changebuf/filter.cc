//#define XERR
#include "changebuf.ih"

bool ChangeBuf::filter(char const **srcBegin, char const **srcEnd)
{
    d_buffer.clear();

    if (not getline(d_in, d_buffer))
        return false;

    d_buffer += '\n';

    if (size_t pos = d_buffer.find(d_search); pos != std::string::npos)
        d_buffer.replace(pos, d_search.length(), d_replace);

    *srcBegin = d_buffer.data();
    *srcEnd = d_buffer.data() + d_buffer.size();
     
    return true;
}
