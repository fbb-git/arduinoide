//#define XERR
#include "changebuf.ih"

ChangeBuf::ChangeBuf(string const &search, string const &replace)
:
    d_search(search),
    d_replace(replace)
{}
