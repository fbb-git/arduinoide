#ifndef INCLUDED_CHANGEBUF_H_
#define INCLUDED_CHANGEBUF_H_

#include <iosfwd>
#include <string>
#include <fstream>

#include <bobcat/ifilterbuf>

class ChangeBuf: public FBB::IFilterBuf
{
    protected:
        std::string d_inName;       // name of the stream to read from
        std::ifstream d_in;         // stream to read from

        std::string d_buffer;       // locally buffered chars

    private:    
        std::string d_search;      
        std::string d_replace;      

        size_t const d_maxSize = 1000;

    public:
        ChangeBuf(std::string const &search,
                                     std::string const &replace);
        void useIstream(std::string const &src);

        std::string const &search() const;
        std::string const &replace() const;

    private:
        bool filter(char const **srcBegin,
                    char const **srcEnd) override;
};

inline std::string const &ChangeBuf::search() const
{
    return d_search;
}

inline std::string const &ChangeBuf::replace() const
{
    return d_replace;
}

#endif
