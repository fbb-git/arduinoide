#define XERR
#include "main.ih"

namespace
{
    Arg::LongOption longOpts[] =
    {
        Arg::LongOption{"clean", Arg::None},
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"include", 'I'},
        Arg::LongOption{"lib", 'l'},
        Arg::LongOption{"src", 's'},
        Arg::LongOption{"verbose", 'V'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOpts + size(longOpts);
}


int main(int argc, char **argv)
try
{
    FS::setCwd();

    Arg const &arg = Arg::initialize("hI:l:s:Vv", longOpts, longEnd, 
                                                argc, argv);
    arg.versionHelp(usage, Icmbuild::version, 0);

    Options options;

    if (arg.nArgs() == 0)
        usage(arg.basename());

    Specifications specs{ arg[0] };

    Headers headers(options, specs);    // create the headers directory/ies
    headers.install();                  // install the headers

    Sources sources(options, specs);    // handle the source files
    sources.compile();

    Library library(options);
    library.build();
}
catch (int nr)
{
    return Arg::instance().option("hv") ? 0 : nr;
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}
