//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_(" [options] specs
Where:
    [options] - optional arguments (short options between parentheses):
        --clean         - remove the src (./src) and headers (./arduino) 
                          directories and terminate 
        --help (-h)     - provide this help and terminate
        --include (-I)  - the include directory to contain the headers
                          (by default ./arduinoide, which should be accessible 
                           as /usr/local/include/arduino)
        --lib (-l)      - the directory to contain the library
                          libarduinoide.a (by default ./lib)
        --src (-t)      - the directory to contain the source files (by 
                          default ./src)
        --version (-v)  - show version information and terminate
    
    specs - file describing the installation specifications.

specifications start with an action specification followed by an arduino
source directory name. E.g., 
    std /usr/share/arduino/hardware/arduino/cores/arduino

Supported actions are:
    std: headers are made available in the include directory (e.g.,
         ./arduinoide) as soft links to their source locations;
         source files are made available in the source directory (e.g., ./src)
         as soft links to their source locations
)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;

    throw 0;
}
