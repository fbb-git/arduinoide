//#define XERR
#include "tftchangebuf.ih"

void TFTChangeBuf::findDefine()
{
    if (d_buffer.find("#define") == 0)
    {
        d_buffer += "\n"
                    "#include <SD.h>\n";
        d_action = &TFTChangeBuf::copyReplace;
    }
}
