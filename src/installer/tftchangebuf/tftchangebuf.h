#ifndef INCLUDED_TFTCHANGEBUF_
#define INCLUDED_TFTCHANGEBUF_

#include "../changebuf/changebuf.h"

class TFTChangeBuf: public ChangeBuf
{
    enum Action
    {
        FIND_DEFINE,
        COPY,
        RM3RD__SD_H__
    };

    bool d_adaFruit = false;
    size_t d_sdHcount = 0;
    void (TFTChangeBuf::*d_testAdaFruit)();
    void (TFTChangeBuf::*d_action)();
                                    // changes to copyReplace once the include
                                    // guard's #define has been seen
    public:
        TFTChangeBuf(std::string const &search,
                                     std::string const &replace);
    private:
        bool filter(char const **srcBegin,
                    char const **srcEnd) override;

        void findDefine();
        void copyReplace();

        void testAdaFruit();
        void nop();
};

#endif
