//#define XERR
#include "tftchangebuf.ih"

void TFTChangeBuf::testAdaFruit()
{
    d_adaFruit = d_inName.find("Adafruit_GFX.h") != string::npos;
    d_testAdaFruit = &TFTChangeBuf::nop;
}
