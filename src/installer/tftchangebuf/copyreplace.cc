//#define XERR
#include "tftchangebuf.ih"

void TFTChangeBuf::copyReplace()
{
    if (size_t pos = d_buffer.find(search()); pos != std::string::npos)
        d_buffer.replace(pos, search().length(), replace());
    else if (
        d_adaFruit
        and 
        d_buffer.find("#if defined(__SD_H__)") != string::npos
        and 
        ++d_sdHcount == 3
    )
    {
        string line;                    // skip all lines thru #endif
        while (getline(d_in, line) and line.find("#endif") == string::npos)
            ;

        getline(d_in, d_buffer);
    }
}
