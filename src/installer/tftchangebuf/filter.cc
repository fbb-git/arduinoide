//#define XERR
#include "tftchangebuf.ih"

bool TFTChangeBuf::filter(char const **srcBegin, char const **srcEnd)
{
    d_buffer.clear();

    (this->*d_testAdaFruit)();

    if (not getline(d_in, d_buffer))
    {
        d_action = &TFTChangeBuf::findDefine;
        d_testAdaFruit = &TFTChangeBuf::testAdaFruit;
        d_adaFruit = false;
        d_sdHcount = 0;
        return false;
    }

    (this->*d_action)();               // initially find_define

    d_buffer += '\n';

    *srcBegin = d_buffer.data();
    *srcEnd = d_buffer.data() + d_buffer.size();
     
    return true;
}
