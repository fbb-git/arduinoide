//#define XERR
#include "tftchangebuf.ih"

TFTChangeBuf::TFTChangeBuf(std::string const &search,
                           std::string const &replace)
:
    ChangeBuf(search, replace),
    d_testAdaFruit(&TFTChangeBuf::testAdaFruit),
    d_action(&TFTChangeBuf::findDefine)
{}
