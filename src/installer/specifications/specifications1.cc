//#define XERR
#include "specifications.ih"

Specifications::Specifications(char const *specName)
//:
{
    ifstream in{ Exception::factory<ifstream>(specName) };

    string line;

    while (getline(in, line))
    {
        ++d_lineNr;
        interpret(String::trim(line));
    }
}
