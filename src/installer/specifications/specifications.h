#ifndef INCLUDED_SPECIFICATIONS_
#define INCLUDED_SPECIFICATIONS_

#include <string>
#include <vector>
#include <unordered_map>

struct Specifications
{
    enum Type
    {
        STD,
        UTILITY2DEST,
        UTIL,
        UTILITYCOMPILE,
        TFTSPECIAL,
    };

    struct Spec
    {
        Type type;        
        std::string path;
    };

    private:
        size_t d_lineNr = 0;
        std::vector<Spec> d_spec;

        static std::unordered_map<std::string, Type> s_key2type;

    public:
        Specifications(char const *specName);
        size_t size() const;                        // # specifications
        Spec const &operator[](size_t idx) const;   // get a specification

    private:
        void interpret(std::string const &line);    // line fm specs
        void path(std::istream &in, Type type);

};
        
inline size_t Specifications::size() const
{
    return d_spec.size();
}

inline Specifications::Spec const 
                            &Specifications::operator[](size_t idx) const
{
    return d_spec[idx];
}

#endif
