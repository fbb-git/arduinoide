//#define XERR
#include "specifications.ih"
                                                            // trimmed line fm
void Specifications::interpret( std::string const &line)    // specs
{                                                       

    if (line.empty() || line[0] == '#')             // ignore empty/comment
        return;

    istringstream in{ line };                       // interpret line content

    string item;
    in >> item;                                     

    auto iter = s_key2type.find(item);
    
    if (iter == s_key2type.end())
        throw Exception{} << "Line " << d_lineNr << ": specification in `" << 
                            line << "' not supported";

    return path(in, iter->second);
}




