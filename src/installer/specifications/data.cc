//#define XERR
#include "specifications.ih"

unordered_map<string, Specifications::Type> Specifications::s_key2type =
{
    { "std"s,               STD             },
    { "utility2dest"s,      UTILITY2DEST    },
    { "util"s,              UTIL            },
    { "utilityCompile"s,    UTILITYCOMPILE  },
    { "tftSpecial"s,        TFTSPECIAL      },
};
