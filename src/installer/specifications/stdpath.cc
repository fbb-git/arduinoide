//#define XERR
#include "specifications.ih"

void Specifications::path(istream &in, Type type)
{
    string path;

    in >> path;

    if (not is_directory(path))
        throw Exception{} << "Line " << d_lineNr << ": non-existing path `" <<
                            path << "'\n";

    d_spec.push_back( { type, path } );
}
