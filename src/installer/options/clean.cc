//#define XERR
#include "options.ih"

void Options::clean() const
{
    removeDir(d_src);
    removeDir(d_include);
    removeDir(d_lib);

    throw 0;
}
