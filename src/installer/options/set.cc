//#define XERR
#include "options.ih"

// static
void Options::set(char const *dir, string &dest)
{
    mkdir(dest);

    if (access(dest.c_str(), R_OK | W_OK | X_OK) != 0)
        throw Exception{} << "no write permissions for dir. `" << 
                                                            dest << '\'';
}
