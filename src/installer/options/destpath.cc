//#define XERR
#include "options.ih"

string Options::destPath(string const &srcPath) const
{
                                            // a subdir of 'sourcePath'
    return d_src + srcPath.substr(srcPath.rfind('/')); 
}
