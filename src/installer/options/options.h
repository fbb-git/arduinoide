#ifndef INCLUDED_OPTIONS_
#define INCLUDED_OPTIONS_

#include <string>

#include "../fs/fs.h"

class Options: private FS
{
    bool d_verbose;
    std::string d_cwd;
    std::string d_src;              // path to the tmp dir
    std::string d_lib;              // path to the lib dir
    std::string d_include;          // path to the include directory

    static std::string s_src;       // path to the tmp dir
    static std::string s_include;   // path to the include directory
    static std::string s_lib;       // path to the lib directory

    public:
        Options();
        std::string const &includePath() const;
        std::string const &libPath() const;
        std::string const &sourcePath() const;
        std::string destPath(std::string const &srcPath) const;

        bool verbose() const;

    private:
        static void set(char const *dir, std::string &dest);

        void clean() const;
};

inline std::string const &Options::includePath() const
{
    return d_include;
}
        
inline std::string const &Options::sourcePath() const
{
    return d_src;
}

inline std::string const &Options::libPath() const
{
    return d_lib;
}

inline bool Options::verbose() const
{
    return d_verbose;
}
        
#endif

