#define XERR
#include "options.ih"

Options::Options()
:
    d_cwd(cwd()),
    d_src(d_cwd + s_src),
    d_lib(d_cwd + s_lib),
    d_include(d_cwd + s_include)
{
    Arg const &arg = Arg::instance();

    d_verbose = arg.option('V');

    string value;

    if (arg.option(&value, 'l'))
        d_lib = value;
    set("lib", d_lib);

    if (arg.option(&value, 't'))
        d_src = value;
    set("src", d_src);

    if (arg.option(&value, 'I'))
        d_include = value;
    set("include", d_include);

    if (arg.option(0, "clean"))
        clean();
}
