//#define XERR
#include "sources.ih"

tuple<std::string, std::string, std::string> Sources::utilLinks(
                        string const &srcPath) const
{
    string dest = sourceLinks(srcPath);

    string utilityDest = sourceLinks(srcPath, "/utility");

    return  {
                dest, utilityDest, 
                " -I" + d_options.includePath() + 
                    "/util" + srcPath.substr(srcPath.rfind('/') + 1) 
            };

}
