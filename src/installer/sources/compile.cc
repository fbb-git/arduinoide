#define XERR
#include "sources.ih"

void Sources::compile()
{
    for (size_t idx = 0, end = d_specs.size(); idx != end; ++idx)
    {
        auto const &spec = d_specs[idx];

        ( this->*s_compile[spec.type] )(spec.path); // STD -> standard
    }
}
