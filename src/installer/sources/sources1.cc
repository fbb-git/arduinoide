//#define XERR
#include "sources.ih"

Sources::Sources(Options const  &options, Specifications const &specs)
:
    d_specs(specs),
    d_options(options)
{}
