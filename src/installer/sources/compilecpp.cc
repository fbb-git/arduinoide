//#define XERR
#include "sources.ih"

void Sources::compileCpp(string const &destDir, 
                         string const &extraOptions) const
{
    cout << "Compiling .cpp files in " << destDir << '\n';

    cd(destDir);                // change to the dest. dir

    Glob srcGlob{ Glob::SYMBOLIC_LINK, "*.cpp", Glob::NOMATCH };

    for (string file: srcGlob)
    {
        string oFile(file);
        oFile.replace(oFile.length() - 3, 3, 1, 'o');       // .cpp -> .o
        
        if (younger(file, oFile))
            avr_gpp(file, extraOptions);
    }
}
