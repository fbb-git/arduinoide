//#define XERR
#include "sources.ih"

void Sources::tftSpecial(string const &srcPath) const
{
    auto &&[dest, utilityDest, extraOptions] =  utilLinks(srcPath);

    ofstream out = Exception::factory<ofstream>(
                                        utilityDest + "/AdafruitST.cpp");
    out << "typedef unsigned char const prog_uchar;\n";

    string stPath = utilityDest + "/Adafruit_ST7735.cpp";

    ifstream in =  Exception::factory<ifstream>(stPath);
    out << in.rdbuf();

    remove(stPath);
    
    compileSources(dest, extraOptions);
    compileSources(utilityDest, extraOptions);
}
