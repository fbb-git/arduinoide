#ifndef INCLUDED_SOURCES_
#define INCLUDED_SOURCES_

#include <iosfwd>

#include "../fs/fs.h"

class Specifications;
class Options;
 
class Sources: private FS
{
    Specifications const &d_specs;
    Options const &d_options;

    static void (Sources::*s_compile[])(std::string const &path) const;

    public:
        Sources(Options const  &options, Specifications const &specs);

        void compile();

    private:

        void standard(std::string const &srcPath) const;
        void utility2dest(std::string const &srcPath) const;

        void util(std::string const &srcPath) const;
        void utilityCompile(std::string const &srcPath) const;

        void tftSpecial(std::string const &path) const;

        std::string sourceLinks(std::string srcPath, 
                                char const *subdir = "") const;

        std::tuple<std::string, std::string, std::string> utilLinks(
                                std::string const &srcPath) const;
        
            // when specifying extraOptions start with a space
        void compileSources(std::string const &destPath,
                            std::string const &extraOptions = "") const;

        void compileCpp(std::string const &destDir, 
                     std::string const &extraOptions) const;
        void avr_gpp(std::string const &srcFile, 
                     std::string const &extraOptions) const;

        void compileC(std::string const &destDir, 
                     std::string const &extraOptions) const;
        void avr_gcc(std::string const &srcFile, 
                     std::string const &extraOptions) const;

        void compileProcess(std::string const &avrCompiler, 
                            std::string const &options, 
                            std::string const &srcFile) const;
};
        
#endif


