//#define XERR
#include "sources.ih"

void Sources::utility2dest(std::string const &srcPath) const
{
    string dest = d_options.destPath(srcPath);  // the ./src/Classname dir

    mkdir(dest);

    softLinks(srcPath + "/*.*", dest,           // link all its files to dest
              d_options.verbose());

    string utilityPath = srcPath + "/utility";  // the srcPath/utility dir

    softLinks(srcPath + "/utility/*.*", dest,   // link all its files to dest
              d_options.verbose());

    standard(dest);                             // and compile the dest files
}
