//#define XERR
#include "sources.ih"

void Sources::compileC(string const &destDir, 
                       string const &extraOptions) const
{
    cout << "Compiling .c files in " << destDir << '\n';

    cd(destDir);                // change to the dest. dir

    Glob srcGlob{ Glob::SYMBOLIC_LINK, "*.c", Glob::NOMATCH };

    for (string file: srcGlob)
    {
        string oFile(file);
        oFile.back() = 'o';                     // .c -> .o
        
        if (younger(file, oFile))
            avr_gcc(file, extraOptions);
    }
}
