//#define XERR
#include "sources.ih"

void Sources::compileProcess(
                    string const &avrCompiler, std::string const &options, 
                    string const &srcFile) const 
{
    Process process{
            Process::NONE,
            "/usr/bin/" + avrCompiler + options + ' ' + srcFile
        };

    if (d_options.verbose())
        cout << avrCompiler << " -c " << srcFile << '\n';

    process.start();

    if (process.waitForChild() != 0)
        throw Exception{} << "compilation of `" << srcFile << "' failed";
}
