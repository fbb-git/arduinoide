//#define XERR
#include "sources.ih"

void (Sources::*Sources::s_compile[])(string const &path) const = 
{
    &Sources::standard,                     // STD
    &Sources::utility2dest,                 // UTILITY2DEST
    &Sources::util,                         // UTIL
    &Sources::utilityCompile,               // UTILITYCOMPILE
    &Sources::tftSpecial,                   // TFTSPECIAL
};
