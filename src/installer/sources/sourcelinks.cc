#define XERR
#include "sources.ih"

string Sources::sourceLinks(string srcPath, char const *subdir) const
{
    string dest = d_options.destPath(srcPath);  // std. srcPath destination

    dest += subdir;                             // add a subdir (may be "")
    srcPath += subdir;                          // also to the srcPath

    mkdir(dest);                                // make the destination dir

                                                // create source soft-links
    softLinks(srcPath + "/*.cpp", dest, d_options.verbose());
    softLinks(srcPath + "/*.c",   dest, d_options.verbose());

    return dest;
}
