//#define XERR
#include "sources.ih"

void Sources::utilityCompile(string const &srcPath) const
{
    auto &&[dest, utilityDest, extraOptions] = utilLinks(srcPath);

    softLinks(srcPath + "/*.h", dest, d_options.verbose());
    softLinks(srcPath + "/utility/*.h", utilityDest, d_options.verbose());

    compileSources(dest, extraOptions);
    compileSources(utilityDest, extraOptions);
}

