//#define XERR
#include "sources.ih"

void Sources::util(string const &srcPath) const
{
    auto &&[dest, utilityDest, extraOptions] =  utilLinks(srcPath);

    compileSources(dest, extraOptions);
    compileSources(utilityDest, extraOptions);
}
