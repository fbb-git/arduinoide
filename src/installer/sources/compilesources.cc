//#define XERR
#include "sources.ih"

void Sources::compileSources(string const &destDir,
                             string const &extraOptions) const
{
    compileCpp(destDir, extraOptions);
    compileC(destDir, extraOptions);
}
