//#define XERR
#include "sources.ih"

namespace {

    // warnings are explicitly switched off: this is not our software

char const options[] = 
    "-c -w -Os -ffunction-sections -fdata-sections -mmcu=atmega328p "
    "-DF_CPU=16000000L -MMD -DUSB_VID=null -DUSB_PID=null "
    "-DARDUINO=101 -fdiagnostics-color=never -I";

}

void Sources::avr_gcc(string const &srcFile, string const &extraOptions) const
{
    compileProcess("avr-gcc ", 
                    options + d_options.includePath() + extraOptions, 
                    srcFile);
}
