//#define XERR
#include "sources.ih"

namespace {

    // warnings are explicitly switched off: this is not our software
char const options[] = 
    "-c -w -Os -fno-exceptions -ffunction-sections "
    "-fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -MMD "
    "-DUSB_VID=null -DUSB_PID=null -std=c++17 -DARDUINO=101 "
    "-fdiagnostics-color=never -I";

}

void Sources::avr_gpp(string const &srcFile, string const &extraOptions) const
{
    compileProcess("avr-g++ ", 
                    options + d_options.includePath() + extraOptions, 
                    srcFile);
}



