#ifndef INCLUDED_LIBRARY_
#define INCLUDED_LIBRARY_

#include <string>

#include "../fs/fs.h"

class Options;

class Library: public FS
{
    Options const &d_options;
    std::string d_libPath;
    std::string d_sourcePath;
    size_t d_dir;
    std::string d_dirStr;

    public:
        Library(Options const &options);
        void build();

    private:
        void copy2libdir(std::string const &src);
};
        
#endif
