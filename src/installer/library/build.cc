//#define XERR
#include "library.ih"

#include <filesystem>
using namespace filesystem;

void Library::build()
{
    cd(d_sourcePath);
    d_dir = 0;

    for (auto src: Glob{ Glob::DIRECTORY, "*" })
        copy2libdir(src);
        
    cd(d_libPath);
    Process proc{ Process::IGNORE_COUT_CERR,  
                  Process::USE_SHELL, "/usr/bin/ar r libarduinoide.a *.o" };
    proc.start();
    if (int err = proc.waitForChild(); err != 0)
        throw Exception{} << "ar failed with error " << err << ": " <<
                                                                errnodescr;
   
    for (auto const &src: Glob{ Glob::REGULAR_FILE, "*.o" })
        remove(src);

    cout << "constructed " << d_libPath << "/libarduinoide.a\n";
}




