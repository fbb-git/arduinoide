//#define XERR
#include "library.ih"

void Library::copy2libdir(string const &srcDir)
{
    if (srcDir[0] == '.')
        return;

    d_dirStr = '/' + to_string(++d_dir);
    cd(srcDir);

        for (string src: Glob{ Glob::REGULAR_FILE, "*.o", Glob::NOMATCH })
            copyFile(src, d_libPath + d_dirStr + src, d_options.verbose(),
                    true);

        for (auto const &dir: Glob{ Glob::DIRECTORY, "*", Glob::NOMATCH })
            copy2libdir(dir);

    cd("..");
}
