//#define XERR
#include "library.ih"

Library::Library(Options const &options)
:
    d_options(options),
    d_libPath(options.libPath()),
    d_sourcePath(options.sourcePath())
{
    mkdir(d_libPath);
}
