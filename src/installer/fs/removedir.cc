//#define XERR
#include "fs.ih"

// static
void FS::removeDir(string const &path)
{
    error_code ec;
    remove_all(path, ec);
    if (ec.value() != 0)
        throw Exception{} << "cannot rm -r directory `" << path << '\'';
}
