//#define XERR
#include "fs.ih"

// static
void FS::copyFile(string const &source, std::string const &dest,
                  bool verbose, bool forced)
{
    if (
        not forced and exists(dest) 
        and not equivalent(source, dest)
    )
        throw Exception{} << dest << " already exists"; 

    error_code ec;
    copy_options opts;
    if (forced)
        opts = copy_options::overwrite_existing;

    copy_file(source, dest, opts, ec);

    if (ec.value() != 0)
        throw Exception{} << "cannot copy " << source;

    if (verbose)
        cout << "copied " << source << " to " << dest << '\n';
}
