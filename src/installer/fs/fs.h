#ifndef INCLUDED_FS_
#define INCLUDED_FS_

#include <iosfwd>
#include <string>

namespace FBB
{
    class Glob;
}

class FS
{
    static std::string s_cwd;

    public:
        static void setCwd();

        static void cd(std::string const &destDir);
        static std::string const &cwd();

        static void mkdir(std::string const &dir);  // dir can be a path: 
                                            // all earlier dirs are created 
                                            // if not yet existing

        static void removeDir(std::string const &path);     // rm -r path

                                            // all files in sourcepath are
                                            // soft links in destDir: at(0):
                                            // # files, at(1): #links
        static size_t softLinks(std::string const &sourcePathPattern,   // 1
                        std::string const &destDir, bool verbose);

        static void copyFile(std::string const &source,
                        std::string const &dest, bool verbose, 
                        bool forced = false);
    
        static size_t copyFiles(std::string const &sourcePathPattern,   // 1
                        std::string const &destDir, bool verbose);
    
                                    // true if objFile doesn't exist or
                                    // srcFile has a more recent time stamp
        static bool younger(std::string const &srcFile, 
                            std::string const &objFile);

        static void remove(std::string const &srcFile);

    private:
        static size_t copyFiles(FBB::Glob const &sourceGlob,            // 2
                              std::string const &destDir, bool verbose);
    
        static size_t softLinks(FBB::Glob const &sourceGlob,            // 2
                                std::string const &destDir, bool verbose);
    
        static void mkdirPath(std::string const &dir);
};
        
// static
inline std::string const &FS::cwd()
{
    return s_cwd;
}

#endif


