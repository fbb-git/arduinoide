//#define XERR
#include "fs.ih"

// static
size_t FS::copyFiles(Glob const &sourceGlob, std::string const &destDir,
                    bool verbose)
{
    for (string src: sourceGlob)
    {
        string fileName{ src.substr(src.rfind('/')) };
        string destPath{ destDir + fileName };

        if (exists(destPath) and not equivalent(src, destPath))
            throw Exception{} << fileName.substr(1) << 
                    " already exists"; 

        error_code ec;
        copy_file(src, destPath);

        if (ec.value() != 0)
            throw Exception{} << "cannot copy \n" << 
                    "       " << src;

        if (verbose)
            cout << "copied " << fileName.substr(1) << '\n';
    }

    return sourceGlob.size();
}
