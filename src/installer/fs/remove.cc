//#define XERR
#include "fs.ih"

// static
void FS::remove(string const &fileName)
{
    filesystem::remove(fileName);
}
