//#define XERR
#include "fs.ih"

// static
void FS::mkdirPath(string const &path)
{
    if (is_directory(path))          // done here.
        return;

    mkdirPath(path.substr(0, path.rfind('/'))); // create the earlier path
    
    error_code ec;
    create_directory(path, ec);

    if (ec.value() != 0)
        throw Exception{} << "cannot create dir. `" << path << '\'';

}
