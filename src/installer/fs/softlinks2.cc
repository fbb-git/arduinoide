//#define XERR
#include "fs.ih"

// static
size_t FS::softLinks(
                    Glob const &sourceGlob, std::string const &destDir,
                    bool verbose)
{
    for (string src: sourceGlob)
    {
        string fileName{ src.substr(src.rfind('/')) };
        string linkPath{ destDir + fileName };

        if (exists(linkPath))
        {
            if (not equivalent(src, linkPath))
                throw Exception{} << "existing " << fileName.substr(1) << 
                    " does not link to\n" << "       " << src; 
            continue;
        }

        error_code ec;
        create_symlink(src, linkPath, ec);

        if (ec.value() != 0)
            throw Exception{} << "cannot create link to\n" << 
                    "       " << src;
        
        if (verbose)
            cout << fileName.substr(1) << ": soft link to " << src << '\n';
    }

    return sourceGlob.size();
}


