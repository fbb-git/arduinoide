//#define XERR
#include "fs.ih"

// static
size_t FS::softLinks(string const &srcPathPattern, 
                     string const &destDir, bool verbose)
{
    return softLinks(
                Glob{ Glob::REGULAR_FILE, srcPathPattern, Glob::NOMATCH }, 
                destDir, verbose);
}
