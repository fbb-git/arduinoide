//#define XERR
#include "fs.ih"

// static
void FS::mkdir(string const &dir)
{
    mkdirPath(absolute(dir));
}
