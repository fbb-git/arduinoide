//#define XERR
#include "fs.ih"

// static
void FS::cd(string const &destDir)
{
    if (chdir(destDir.c_str()) != 0)
        throw Exception{} << "cannot cd to `" << destDir << ": " <<
                errnodescr << '\'';
}
