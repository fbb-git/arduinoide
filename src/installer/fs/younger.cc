//#define XERR
#include "fs.ih"

// static
bool FS::younger(string const &srcFile, string const &objFile)
{
    return exists(objFile) ?
            Stat{ srcFile }.lastChange() >= Stat{ objFile }.lastChange()
        :
            true;
}
