//#define XERR
#include "fs.ih"

// static
size_t FS::copyFiles(string const &srcPathPattern, 
                     string const &destDir, bool verbose)
{
    return copyFiles(
                Glob{ Glob::REGULAR_FILE, srcPathPattern, Glob::NOMATCH }, 
                destDir, verbose);
}
