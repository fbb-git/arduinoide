/* Calibration sketch for HX711 */
 
#include <Arduino.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>

#include <HX711.h>  // Library needed to communicate with HX711
                    // https://github.com/bogde/HX711 

namespace
{ 
    int const dOut = 3;     // Arduino pin 3 connect to HX711 dOut
    int const clk =  2;     //  Arduino pin 2 connect to HX711 CLK
    int const servoPin = 9; //  Arduino pin 9 steers the servo

    HX711 scale;                
    
    LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 
    
    Servo servo;

    size_t count = 0;
    long lastReading = 0;
    bool updated = true;

    double lastWeight = 0;
}

void setup() 
{
    servo.attach(servoPin);
    servo.write(0);   

    scale.begin(dOut, clk);

    lcd.begin(16, 2);    
    lcd.backlight();

    delay(1000);                // wait for setup (i.e., the servo) to
                                // move to the initial position
}

void loop() 
{
    if (not scale.is_ready()) 
    {
        lcd.clear();
        lcd.setCursor(0, 0);        
        lcd.print("No HX711 found.");
    }

    long reading = 
            -(scale.read() / 5000 + 54);            // stable at 0

    count = (count + 1) % 100;

    if (lastReading != reading)
    {
        updated = true;
        lastReading = reading;
    }
    else if (updated)
    {
        updated = false;

        lcd.clear();
        lcd.setCursor(0, 0);        

        lcd.print(count);

        
        lcd.setCursor(0, 1);        // col 0, row 1

        lcd.print("weight: ");
        double weightKg = reading * (11.7188 / 1000);
        lcd.print(weightKg);        // weight in kg

        if (lastWeight < 1.25 and weightKg > 1.25)
        {
            lastWeight = weightKg;
            servo.write(180);
        }
        if (lastWeight > 1.25 && weightKg < 1.25)
        {
            lastWeight = weightKg;
            servo.write(0);
        }
    }

    delay(250);
}




