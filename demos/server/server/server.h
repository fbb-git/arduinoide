#ifndef INCLUDED_SERVER_
#define INCLUDED_SERVER_

#include <iosfwd>
#include <bobcat/serversocket>
#include <bobcat/fork>
#include <bobcat/signal>

class Server: public FBB::Fork, public FBB::SignalHandler
{
    enum LockingState
    {
        UNLOCKED,
        LOCKED
    };

    enum Action
    {
        NO_ACTION = '-',
        LOCK = 'L',
        UNLOCK = 'U'
    };

    enum State
    {
        START,
        LOW_WEIGHT,
        HIGH_WEIGHT,
        OUR_CAT,
        OTHER_CAT,
        NOT_ACTIVE,
    };
    
    std::string d_imagePath;
    std::string d_wgetCommand;

    bool d_analyze;

    FBB::ServerSocket d_serverSocket;

    std::string d_pidFile;

    unsigned d_morning;                 // no lock beyond this time    
    unsigned d_evening;                 // no lock beforeb this time    
    unsigned d_requiredCount;           // required Image::analysis count 
                                        // to decide it's our cat

    double d_checkWeight;           // weights > checkWeight: maybe close the
                                    //  gate
    double d_openWeight;            // weight < openWeight: open the gate


    LockingState d_lockingState = UNLOCKED; // the gate's locking state
            

    void (ImageHandler::*d_useName)(std::string &renamedImg); // maybe rename

    bool d_quit = false;

    Image d_image;

    std::ostream *d_in  = 0;            // not allocated: initialized in run()
    std::ostream *d_out = 0;

    State d_state;                      // initialized at process()
    static bool (Server::*s_state[])();

    static unsigned s_requiredCount;    
    static unsigned s_morning;          // no lock beyond this time    
    static unsigned s_evening;          // no lock before this time    

    static double s_checkWeight;    // default checkWeight
    static double s_openWeight;     // default openWeight

    static size_t s_port;
    static char const s_pidFile[];

    static char (Server::*const s_process[])(double value); // process value
                                            // given the current locking state

    public:
        Server(bool analyze);
        void run();                         // calls process for normal use

        static double checkWeight();        // default weights and port
        static double openWeight();
        static size_t port();
        static char const *pidFile();

    private:
        void signalHandler(size_t signum) override;
        void childProcess() override;
        void parentProcess() override;

        void process(int fd);               // process arduino's info

                                            // states handled by process

        bool start();                       //OK
        bool lowWeight();                   //OK
        bool highWeight();
        bool ourCat();
        bool otherCat();
        bool notActive();

        State inspectCat();                 // OUR_CAT or OTHER_CAT

        static bool active();               //OK
        bool isActive();                    //OK if not active: log, NOT_ACTIVE
        void weightState(double value);     //OK log, LOW_WEIGHT/HIGH_WEIGHT

        bool checkHighWeight(double value); //OK log, -> HIGH_WEIGHT
        bool checkLowWeight(double value);  //OK log, -> LOW_WEIGHT

        char notLocked(double value);       // called when not locked
        char locked(double value);          // called when locked.

        static bool getWeight(double &value);   //OK

        static double setWeight(double defaultWeight, int option); 
        static size_t setPort();
        static unsigned evening();
        static unsigned morning();
};

// static
inline double Server::checkWeight()
{
    return s_checkWeight;
}

// static
inline double Server::openWeight()
{
    return s_openWeight;
}

// static
inline size_t Server::port()
{
    return s_port;
}

// static
inline char const *Server::pidFile()
{
    return s_pidFile;
}

// static
inline unsigned Server::evening()
{
    return s_evening;
}
        
// static
inline unsigned Server::morning()
{
    return s_morning;
}

        
#endif







