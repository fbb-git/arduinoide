//#define XERR
#include "server.ih"

void Server::process(int fd)
{
    IFdStream in(fd);           // stream to read from the arduino
    d_in = &in;

    OFdStream out(fd);          // stream to write to the arduino
    d_out = &out;

                                // the arduino sends lines containing 
                                //  one value
    d_state = START;
    while ((this->*s_state[d_state])())
        ;


//    double value;               // value received from the arduino
//    
//    while (in >> value and in.ignore(100, '\n'))
//    {
////        g_log << "received " << value << ", state = " << d_lockingState <<
////                endl;
//
//                                // either 'locked' or 'notLocked'
//        out << (this->*s_process[d_lockingState])(value) << flush;
//    }
}   

