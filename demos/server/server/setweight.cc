//#define XERR
#include "server.ih"

// static
double Server::setWeight(double defaultWeight, int option)
{
    string value;
    return Arg::instance().option(&value, option) ? 
                stod(value) 
            : 
                defaultWeight;
}
