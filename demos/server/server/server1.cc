//#define XERR
#include "server.ih"

Server::Server(bool analyze)
:
    d_analyze(analyze),
    d_serverSocket( setPort() ),
    d_imagePath(setImagePath()),
    d_morning(setValue(s_morning, 'm')),
    d_evening(setValue(s_evening, 'e')),
    d_checkWeight(setWeight(s_checkWeight, 'c')),
    d_openWeight(setWeight(s_openWeight, 'o')),
    d_imageHandler(d_analyze)
{
    if (d_analyze)
        return;

    if (not Arg::instance().option(&d_pidFile, 'P'))
        d_pidFile = s_pidFile;

    Signal::instance().add(SIGTERM, *this);     // std kill signal (15)
    Signal::instance().add(SIGINT, *this);      // ^C signal (2)

    g_log << "server: " <<
        "address = " << d_serverSocket.dottedDecimalAddress() << ", "
        "port = " << d_serverSocket.port() << endl <<   // endl is used to
                                                        // force a timestamp
                                                        // on the next line
        "check weight = " << d_checkWeight << ", "
        "open weight = " << d_openWeight << endl;
}
