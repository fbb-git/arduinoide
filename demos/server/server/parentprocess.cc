//#define XERR
#include "server.ih"

void Server::parentProcess() // overrides
{
    ofstream runPid = Exception::factory<ofstream>(d_pidFile);

    runPid << pid() << endl;
}
