//#define XERR
#include "server.ih"

// static
bool Server::active() 
{
    unsigned hours = DateTime{ DateTime::LOCALTIME }.hours();

    return d_morning < hours and hours < d_evening;
}
