//#define XERR
#include "server.ih"

char Server::locked(double value)        // called when the gate is locked
{
    if (value > d_openWeight)
    {
        g_log << "locked: ignoring " << value << endl;
        return NO_ACTION;                // arduino can ignore this weight
    }

    d_lockingState = UNLOCKED;
    g_log << "opening the gate at weight " << value << endl;

    return UNLOCK;
}

