//#define XERR
#include "server.ih"

bool Server::lowWeight(istream &in, ostream &out)
{
    double value;                       // value received from the scale

    if (not (in >> value and in.ignore(100, '\n'))
        return false;

    if (value >= d_checkWeight)
    {
        g_log << "received high weight " << value << endl;
        d_state = HIGH_WEIGHT;
    }

    return true;
}
