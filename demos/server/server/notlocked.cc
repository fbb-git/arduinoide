//#define XERR
#include "server.ih"

char Server::notLocked(double value)        // called when the gate is open
{
    if (value < d_checkWeight)              // too light
    {
        g_log << "unlocked: ignoring " << value << endl;
        return NO_ACTION;
    }

    if (not d_imageHandler.lock(value))     // or the image handler says
        return NO_ACTION;                   // don't lock, then
                                            // arduino can ignore this weight

    d_lockingState = LOCKED;
    g_log << "locking the gate at weight " << value << endl;

    return LOCK;
}
