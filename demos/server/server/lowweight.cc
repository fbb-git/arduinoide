//#define XERR
#include "server.ih"

bool Server::lowWeight()
{
    double value;                           // value received from the scale

    if (not getWeight(value))
        return false;

    if (isActive())
        checkHighWeight();

    return true;
}
