//#define XERR
#include "server.ih"

void Server::signalHandler(size_t signal)
{
    g_log << "program ends at signal " << signal << endl;

    d_quit = true;

    throw 0;
}
