//#define XERR
#include "server.ih"

bool Server::isActive() 
{
    bool operational = active();

    if (not operational)
    {
        d_state = NOT_ACTIVE;
        g_log << "not active" << endl;
    }

    return operational;
}
