//#define XERR
#include "server.ih"

bool Server::checkLowWeight(double value)
{
    if (value > d_openWeight)
        return false;

    g_log << "received low weight " << value << endl;
    d_state = LOW_WEIGHT;

    return true;
}
