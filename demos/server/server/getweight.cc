//#define XERR
#include "server.ih"

// static
bool Server::getWeight(double &value)
{
    return *d_in >> value and *d_in.ignore(100, '\n');
}
