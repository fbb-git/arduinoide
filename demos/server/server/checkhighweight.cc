//#define XERR
#include "server.ih"

bool Server::checkHighWeight(double value)
{
    if (value < d_checkWeight)
        return false;

    g_log << "received high weight " << value << endl;
    d_state =  HIGH_WEIGHT;

    return true;
}
