//#define XERR
#include "server.ih"

bool Server::notActive((istream &in, ostream &out)
{
    double value;

    if (not getWeight(in, value))
        return false;

    if (active()
        weightState(value);

    return true;
}
