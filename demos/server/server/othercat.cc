//#define XERR
#include "server.ih"

bool Server::otherCat(istream &in)
{
    double value;                           // value received from the scale

    if (not getWeight(in, value))
    {
        d_out << UNLOCK << flush;
        return false;
    }

    if (not isActive() or checkLowWeight())
        out << UNLOCK << flush;
    
    return true;
}


