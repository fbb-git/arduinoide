//#define XERR
#include "server.ih"

bool Server::getImage()
{
    if (not Exec{}.execute(d_wgetCommand))
    {
        g_log << "failed to wget image " << d_imagePath << endl;
        return false;
    }

    return true;
}
