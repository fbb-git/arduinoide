//#define XERR
#include "server.ih"

double Server::s_checkWeight = 3;
double Server::s_openWeight = 0.1;

unsigned Server::s_morning = 7;
unsigned Server::s_evening = 22;

size_t Server::s_port = 7750;

char const Server::s_pidFile[] = "/tmp/imgserver.pid";

char (Server::*const Server::s_process[])(double value) 
{
    &Server::notLocked,
    &Server::locked
};

bool (Server::*Server::s_state[])() =
{
        &Server::start,
        &Server::lowWeight,
        &Server::highWeight,
        &Server::ourCat,
        &Server::otherCat,
        &Server::notActive,
};
