//#define XERR
#include "server.ih"

// static
size_t Server::setPort()
{
    string value;
    return Arg::instance().option(&value, 'p') ? stoul(value) : s_port;
}
