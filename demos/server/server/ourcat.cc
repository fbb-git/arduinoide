//#define XERR
#include "server.ih"

bool Server::ourCat(istream &in, ostream &out)
{
    double value;                           // value received from the scale

    if (not getWeight(in, value))
        return false;

    if (isActive())
        checkLowWeight(); 
    
    return true;
}
