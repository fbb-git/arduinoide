//#define XERR
#include "server.ih"

void Server::weightState(double value)
{
    if (not checkLowWeight(value))      // maybe d_state = LOW_WEIGHT
        checkHighWeight(value);         // maybe           HIGH_WEIGHT
}
