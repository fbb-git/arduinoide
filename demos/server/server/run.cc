//#define XERR
#include "server.ih"

void Server::run()
try
{
    if (d_analyze)
    {
        d_imageHandler.analyze();
        return;
    }

    d_serverSocket.listen();

    
    while (true)
    {
        SocketBase base = d_serverSocket.accept();  // accept a connection
        int fd = base.socket();
    
        g_log << "Client FD = " << fd << ", "
                "address = " << base.dottedDecimalAddress() << ", "
                "using port " << base.port() << endl;

        process(fd);                                // process arduino's info

        g_log << "connection ended" << endl;
        
        if (d_quit)
            throw 0;
    }
}
catch (exception const &exc)
{
    g_log << "Server::run: " << exc.what() << endl;
    throw 1;
}





