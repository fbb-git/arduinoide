//#define XERR
#include "server.ih"

void Server::childProcess() // overrides
{
    prepareDaemon();
    run();
}
