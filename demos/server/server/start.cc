//#define XERR
#include "server.ih"

bool Server::start()
{
    double value;                           // value received from the scale

    if (not getWeight(value))
        return false;

    if (isActive())
        weightState(value);
    
    return true;
}
