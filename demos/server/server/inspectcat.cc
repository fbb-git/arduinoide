//#define XERR
#include "server.ih"

void Server::inspectCat(ostream &out)
{
    string renamedImg;

    (this->*d_useName)(renamedImg);

        // color count exceeds d_colorCount? Then it's our cat
    if (d_image.histogram(renamedImg) >= d_requiredCount)
    {
        g_log << "not locking the gate at image " << renamedImg << endl;
        d_state = OUR_CAT;
    }
    else
    {
        g_log << "locking the gate at image " << renamedImg << endl;
        
        out << LOCK << flush;
        d_state = OTHER_CAT;
    }
}
