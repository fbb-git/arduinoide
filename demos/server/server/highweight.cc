//#define XERR
#include "server.ih"

bool Server::highWeight(istream &in, ostream &out)
{
    for (size_t attempt = 0; attempt != 5; ++attempt)
    {
        if (getImage())
        {
            d_state = inspectCat();     // OUR_CAT or OTHER_CAT (-> LOCKS)
            return true;
        }
    }

    g_log << "ALERT: cannot obtain images\n";
    return true;
}
