//#define XERR
#include "support.ih"

unsigned setValue(unsigned defaultTime, int option)
{
    string value;

    return Arg::instance().option(&value, option) ?
                stoul(value)
            :
                defaultTime;
}
