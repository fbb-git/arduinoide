#include "main.ih"

void setLog(bool analyze)
{
    Arg const &arg = Arg::instance();

    if (analyze or arg.option('f'))
    {
        g_log.open("&1");
        return;
    }

    string logName = g_logName;

    arg.option(&logName, 'l');

    g_log.open(logName);

    g_log << arg.basename() << " starts" << endl;
}
