//#define XERR
#include "image.ih"

void Image::copy1jpg(string const &img) const
{
    filesystem::copy_file(img, "/tmp/1.jpg", 
                          filesystem::copy_options::overwrite_existing);
}
