//#define XERR
#include "main.ih"

int main(int argc, char **argv)
try
{
    Image image;
    unsigned value = image.analyze(argv[1]);
    cout << hex << value << '\n';
}
catch(int x)
{
    return x;
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
}
catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}
