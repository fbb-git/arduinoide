#define XERR
#include "image.ih"

unsigned Image::histogram(string const &img)
{
    union Combi
    {
        unsigned value;
        uint8_t field[3];
    };

    (this->*d_get1jpg)(img);

//    Stat stat{ "/tmp/1.jpg" };
//    g_log << "/tmp/1.jpg size: " << stat.size() << endl;


    Process convert{ Process::COUT, 
                    "/usr/bin/convert /tmp/1.jpg  -format %c "
                    "-depth 8 histogram:-" };
    string str;

    convert.start();

    while (true)
    {
        convert >> str;
        if (str.find("comment") == 0)
            break;                      // got the 'comment' line
        convert.ignore(1000, '\n');
    }

    Combi histValue;
    unsigned  max[3] = { 0 };

    while (true)
    {
        unsigned count;
        if (not (convert >> count))     // not a line starting with a nr
        {
            convert.clear();            // clear the error flag,
            break;                      // then done
        }

        if (count < d_frequency)        // low color counts are ignored
        {
            convert.ignore(1000, '\n');
            continue;
        }

        convert.ignore(1000, '#');
        convert >> str;                 // get the color 

        //cout << setw(3) << count << ' ' << str << '\n';

        histValue.value = stoul(str, 0, 16);

        for (unsigned fieldIdx = 0; fieldIdx != 3; ++fieldIdx)
        {
            if (histValue.field[fieldIdx] > max[fieldIdx])
                max[fieldIdx] = histValue.field[fieldIdx];
        }

//        cerr << count << ' ' << hex << stoul(str, 0, 16) << dec<< '\n';

        convert.ignore(1000, '\n');        
    }

    while (convert.ignore(5000, '\n'))
        ;

    g_log << hex << "color components: " << 
                max[2] << ' ' << max[1] << ' ' <<max[0] << dec << endl;

    return (max[0] >= d_colorBorder) + 
           (max[1] >= d_colorBorder) + (max[2] >= d_colorBorder);
}



