#ifndef INCLUDED_IMAGE_
#define INCLUDED_IMAGE_

//#include <vector>
#include <iosfwd>

class Image
{
//    std::vector< std::pair<unsigned, unsigned> > d_histogram;

    unsigned d_colorBorder;
    unsigned d_frequency;
    unsigned d_reductionPercentage;

    void (Image::*d_get1jpg)(std::string const &img) const;

    static unsigned s_colorBorder;
    static unsigned s_reductionPercentage;
    static unsigned s_frequency;

    public:
        Image();

        unsigned histogram(std::string const &img);

        static unsigned colorBorder();
        static unsigned frequency();
        static unsigned reductionPercentage();

    private:

        void reduceTo1jpg(std::string const &img) const;
        void copy1jpg(std::string const &img) const;
};

// static
inline unsigned Image::colorBorder()
{
    return s_colorBorder;
}
        
// static
inline unsigned Image::frequency()
{
    return s_frequency;
}
        
// static
inline unsigned Image::reductionPercentage()
{
    return s_reductionPercentage;
}
        
#endif
