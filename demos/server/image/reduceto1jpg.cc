//#define XERR
#include "image.ih"

void Image::reduceTo1jpg(string const &img) const
{
    Process reduce{ Process::NONE, 
                    "/usr/bin/convert " + img + 
                    " -resize " + 
                    to_string(d_reductionPercentage) + "% /tmp/1.jpg" };

    g_log << " reducing to " << 
             to_string(d_reductionPercentage) << "% /tmp/1.jpg" << endl;

    reduce.start();
    reduce.waitForChild();
}

