//#define XERR
#include "image.ih"

Image::Image()
:
    d_colorBorder(setValue(s_colorBorder, 'b')),
    d_frequency(setValue(s_frequency, 'f')),
    d_reductionPercentage(setValue(s_reductionPercentage, 'r'))
{
    d_get1jpg = d_reductionPercentage == 100 ?
                    &Image::copy1jpg
                :
                    &Image::reduceTo1jpg;

    g_log << "Color border: " << d_colorBorder << " (0x" << hex <<
                d_colorBorder << dec << ')' << endl <<
            "Image reduction percentage: " << d_reductionPercentage << endl <<
            "Minimum considered pixel frequency: " << d_frequency << endl;
}
