#define XERR
#include "imagehandler.ih"

void ImageHandler::analyze()
{
    d_imagePath = Arg::instance()[0];

    g_log << "Analyzing " << d_imagePath << endl;

    imageType();
}
