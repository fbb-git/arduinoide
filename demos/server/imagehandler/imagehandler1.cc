#define XERR
#include "imagehandler.ih"

ImageHandler::ImageHandler(bool analyze)
:
    d_analyze(analyze),
    d_requiredCount(setValue(s_requiredCount, 'R'))
{
    if (analyze)
    {
        d_getImage = &ImageHandler::dontGetImage;
        d_useName  = &ImageHandler::noRename;
    }
    else       
    {
        d_getImage = &ImageHandler::getImage;
        d_useName  = &ImageHandler::doRename;

        wgetCommand();      // wgetcommand is 1st argument -> stores the file 
                            // at imagePath (--img/-i option)

        g_log << "The gate won't close between " << d_morning << ":00 and " <<
                 d_evening << ":00" << endl;
    }

    g_log << "At least " << d_requiredCount << " color borders must be "
            "exceeded to decide it's our cat" << endl;
}
