#define XERR
#include "imagehandler.ih"

    // true: lock the gate. The image indicates it's not our
    // cat in the defined time interval 

bool ImageHandler::lock(double value)
{
    if (unsigned hours = DateTime{ DateTime::LOCALTIME }.hours();
        d_morning < hours and hours < d_evening
    )
    {
        g_log << "received " << value << ", state: UNLOCKED, " 
                "not active" << endl;
        return false;    
    }

                            // try max. 5 times to obtain a valid
                            // image
    for (size_t attempt = 0; attempt != 5; ++attempt)
    {
        if (not (this->*d_getImage)())  // get an image (or use -f's image)
            continue;                   // (getImage or dontGetImage)

        switch (imageType())            // was: ourCat
        {
            case OURCAT:
            return false;

            case OTHERCAT:
            return true;

            case IMAGE_ERROR:
            break;
        }
    }

    g_log << "ALERT: cannot obtain images\n";
    return false;
}




