//#define XERR
#include "imagehandler.ih"

bool ImageHandler::getImage()
{
    if (not Exec{}.execute(d_wgetCommand))
    {
        g_log << "failed to wget image " << d_imagePath << endl;
        return false;
    }

//    Stat stat{ d_imagePath };
//    g_log << "New " << d_imagePath << " (" << stat.size() << ')' << endl;
    return true;
}
