//#define XERR
#include "imagehandler.ih"

// static
string ImageHandler::setImagePath()
{
    string ret;
    return Arg::instance().option(&ret, 'i') ? ret : s_imagePath;

    
}
