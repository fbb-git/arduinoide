//#define XERR
#include "imagehandler.ih"

void ImageHandler::doRename(string &renamedImg)
{
    renamedImg = d_imagePath + '.' + to_string(d_lockNr++);
    g_log << "processing " << renamedImg << endl;

    error_code ec;
    filesystem::rename(d_imagePath, renamedImg, ec);
}
