//#define XERR
#include "imagehandler.ih"

void ImageHandler::wgetCommand()
{
    char const *fname = Arg::instance()[0];     // get the wget-cmd filename

    ifstream in = Exception::factory<ifstream>(fname);

    if (not getline(in, d_wgetCommand))         // get the wget command
    {
        g_log << "cannot open `" << fname << '\'' << endl;
        throw 1;
    }

    d_wgetCommand += d_imagePath;               // set the image's destination
}
