#ifndef INCLUDED_IMAGEHANDLER_
#define INCLUDED_IMAGEHANDLER_

#include <string>

#include "../image/image.h"

class ImageHandler
{
    enum ImageState
    {
        OURCAT,
        OTHERCAT,
        IMAGE_ERROR                     // somehow can't obtain a processable
                                        // image
    };

    bool d_analyze;                     // only analyze the image
                                        // logs to cout
    Image d_image;


    size_t d_lockNr = 0;                // count #locks, used as extra
                                        // extension of saved images


    unsigned d_morning;                 // no lock beyond this time    
    unsigned d_evening;                 // no lock beforeb this time    

    bool (ImageHandler::*d_getImage)(); // getImage or dontGetImage

    static std::string s_imagePath;

    public:
        ImageHandler(bool analyze);

        bool lock(double value);        // true: lock the gate.
                                        // The image indicates it's not our
                                        // cat in the defined time interval 

        void analyze();                 // directly analyze Arg::instance()[0]

        static std::string const &imagePath();
        static unsigned requiredCount();

    private:
        void wgetCommand();
        bool getImage();                        // true: got an image
        bool dontGetImage();                    // no get needed at -f:
                                                // image specified
        ImageState imageType();

        void noRename(std::string &renamedImg);
        void doRename(std::string &renamedImg);

        static std::string setImagePath();      // return d_imagePath's value
};

// static
inline std::string const &ImageHandler::imagePath()
{
    return s_imagePath;
}

// static
inline unsigned ImageHandler::requiredCount()
{
    return s_requiredCount;
}
                
#endif
