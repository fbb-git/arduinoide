//#define XERR
#include "imagehandler.ih"

ImageHandler::ImageState ImageHandler::imageType()
try
{
    string renamedImg;

    (this->*d_useName)(renamedImg);

        // color count exceeds d_colorCount? Then it's our cat
    if (d_image.histogram(renamedImg) >= d_requiredCount)
    {
        g_log << "not locking the gate at image " << renamedImg << endl;
        return OURCAT;
    }

    g_log << "locking the gate at image " << renamedImg << endl;
    return OTHERCAT;
}
catch (...)
{
    return IMAGE_ERROR;
}



