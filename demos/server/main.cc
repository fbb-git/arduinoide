//#define XERR
#include "main.ih"

namespace
{
    Arg::LongOption longOpts[] =
    {
                                            // direct analysis of an image
        Arg::LongOption{"analyze",  'a' },
        Arg::LongOption{"border",   'b'},   // pixel border color count
        Arg::LongOption{"check",    'c'},   // weight above which the gate 
                                            // may be closed
        Arg::LongOption{"daemon",   'd'},
        Arg::LongOption{"frequency",'f'},
        Arg::LongOption{"evening",  'e'},
        Arg::LongOption{"help",     'h'},
        Arg::LongOption{"img",      'i'},
        Arg::LongOption{"log",      'l'},
        Arg::LongOption{"morning",  'm'},
        Arg::LongOption{"open",     'o'},   // weight below which the gate 
                                            // opens
        Arg::LongOption{"pid",      'P'},   // file containing the server pid
        Arg::LongOption{"port",     'p'},   // port to use
        Arg::LongOption{"reduction",'r'},   // reduction percentage of images
        Arg::LongOption{"required", 'R'},   // required # of border counts 
        Arg::LongOption{"version",  'v'},
    };
    auto longEnd = longOpts + size(longOpts);
}

string g_logName{ "/tmp/arduino.log" }; // with --analyze: logs to cout

Log g_log;                              // global log object

int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize("ab:c:de:f:hi:l:m:o:P:p:r:R:v", 
                                     longOpts, longEnd, argc, argv);
    arg.versionHelp(usage, Icmake::version, 1);

    bool analyze = arg.option('a');

    setLog(analyze);

    Server server{ analyze };

    if (not analyze and arg.option('d'))
        server.fork();
    else
        server.run();
}
catch (int x)
{
    return Arg::instance().option("hv") ? 0 : x;
    return x;
}
catch (exception const &exc)
{
    g_log << exc.what() << '\n';
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    g_log << "unexpected exception\n";
    cerr << "unexpected exception\n";
    return 1;
}



