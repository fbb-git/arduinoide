//                     usage.cc

#include "main.ih"

namespace {
char const border[] = R"_( [options] wget-file
Where:
    [options] - optional arguments (short options between parentheses):
        --analyze           - direct analysis of the image specified as first
                              argument (instead of wget-file). To suppress
                              reduction use --reduce 100
        --border (-b) count - lowest color byte cound to decide that it's
                              our cat (default: )_";

char const check[] = R"_()
        --check (-c) weight - lowest considered weight (kg)for closing the 
                              gate (default: )_";

char const evening[] = R"_()
        --daemon (-d)       - run as a daemon (default: runs in the forground)
        --evening (-e) hour - earliest hour that the gate may lock
                              (default: )_";

char const frequency[] = R"_()
        --frequency (-f) freq - lowest pixel frequency that is considered
                              (default: )_";

char const image[] = R"_()
        --help (-h)         - provide this help
        --img (-i) path     - path to the file (without .<nr> extension) to
                              contain the image to analyze.
                              (default: )_";

char const log[] = R"_()
        --log (-l) logfile  - path to the logfile (default: )_";

char const morning[] = R"_()
        --morning (-m) hour - latest hour that the gate may lock
                              (default: )_";

char const open[] = R"_()
        --open (-o) weight  - weight below which the gate will be opened
                              (default: )_";

char const pid[] = R"_()
        --pid (-P) pidfile  - location of the file containing the server's pid
                              (default: )_";

char const port[] = R"_()
        --port (-p) port    - port to use by the server (default: )_";

char const reduction[] = R"_()
        --reduction (-r) percentage - reduction percentage of images. Use
                              --reduction 100 to use the image as-is.
                              (default: )_";

char const required[] = R"_()
        --required (-R) count - required number of border counts to decide 
                              that it's our cat (default: )_";

char const endInfo[] = R"_()
        --version (-v)      - show version information and terminate

    wget-file - filename containing the full wget-command (ending in -O) 
                as its 1st line. The path to the image file is appended to 
                the -O specification.

)_";

}

void usage(std::string const &progname)
{
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);          // 2 digits behind the .

    cout << "\n" <<
    progname << " by " << Icmake::author << "\n" <<
    progname << " V" << Icmake::version << " " << Icmake::years << "\n"
    "\n"
    "Usage: " << progname << 
                border      << Image::colorBorder()             <<
                               " (0x" << hex << Image::colorBorder() << 
                                                          ')' << dec <<
                check       << Server::checkWeight()            <<
                evening     << ImageHandler::evening() << ":00" <<
                image       << ImageHandler::imagePath()        <<
                frequency   << Image::frequency()               <<
                log         << g_logName                        <<
                morning     << ImageHandler::morning() << ":00" <<
                open        << Server::openWeight()             <<
                pid         << Server::pidFile()                <<
                port        << Server::port()                   <<
                reduction   << Image::reductionPercentage()     <<
                required    << ImageHandler::requiredCount()    <<
                endInfo;
}
