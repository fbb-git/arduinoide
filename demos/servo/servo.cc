#include <Arduino.h>
#include <Servo.h>

Servo servo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

void setup() 
{
    servo.attach(9);      // attaches the servo on pin 9 to the servo object
    servo.write(0);   
    delay(1000);
}

void loop() 
{
    servo.write(180);
    delay(2000);
    servo.write(0);
    delay(2000);    
//        for (int pos = 0; pos <= 180; pos += 1) 
//        {                                   // goes from 0 degrees to 180 
//                                            // degrees in steps of 1 degree
//            servo.write(pos);             // tell servo to go to position in
//                                            // variable 'pos' 
//            delay(5);                      // waits for the servo to reach
//                                            // the position 
//        }
//    
//        for (int pos = 180; pos >= 0; pos -= 1) 
//        {                                   // goes from 180 degrees to 0 
//            servo.write(pos);             // tell servo to go to position in
//                                            // variable 'pos' 
//            delay(5);                      // waits for the servo to reach
//                                            // the position 
//        }
}

