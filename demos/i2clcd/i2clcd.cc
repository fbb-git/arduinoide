/* Blink5
 
 Turns on and off a light emitting diode(LED) connected to a digital pin. 
 After 5x second on-second off the led goes on for 5 secs, after 
 which the cycle repeats
 
 The circuit:
 * LED attached from pin 13 to ground.
 * Note: on most Arduinos, there is already an LED on the board
 that's attached to pin 13, so no hardware is needed for this example.
 
*/

    // use -I/usr/local/include/arduino, and omit the arduino/ prefix before
    // Arduino.h:
#include <Arduino.h>

#include <LiquidCrystal_I2C.h>

int const ledPin =  13;      // the number of the LED pin

long const oneSecond = 1000;       // interval at which to blink (milliseconds)
long const twoSeconds = 2000;
long const fourSeconds = 4000;

int count = 1;              // counts # of iterations: immediately at startup
                            // the light is off, and we wait 1 second in the loop.
int lightCount = 0;         // counts # of times the led has been switched on

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

void setup() {
  // set the digital pin as output:
    pinMode(ledPin, OUTPUT);      
    lcd.begin(16, 2);    
    lcd.backlight();// To Power ON/OFF the back light
}

void loop()
{
    if (++count & 1)
    {
        digitalWrite(ledPin, LOW);
        lcd.clear();
    }
    else
    {
        digitalWrite(ledPin, HIGH);
        lcd.setCursor(0, 0);        
        lcd.print("Zoef en Boljo");
        lcd.setCursor(0, 1);        
        lcd.print("Tijg en Zoem");
    }

    delay(twoSeconds);
    
}




