/* Calibration sketch for HX711 */
 
#include <Arduino.h>

#include <LiquidCrystal_I2C.h>

#include <HX711.h>  // Library needed to communicate with HX711
                    // https://github.com/bogde/HX711 
 
#define DOUT  3  // Arduino pin 3 connect to HX711 DOUT
#define CLK  2  //  Arduino pin 2 connect to HX711 CLK
 
//HX711 scale(DOUT, CLK);  // Init of library

HX711 scale;                

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 


void setup() {
//  Serial.begin(9600);

    scale.begin(DOUT, CLK);

    lcd.setCursor(0, 0);        
    lcd.print("Starting");

//  scale.set_scale();  // Start scale
//  scale.tare();       // Reset scale to zero


    lcd.begin(16, 2);    
    lcd.backlight();

}

size_t count = 0;
long lastReading = 0;
bool updated = true;

void loop() 
{
//
//    double scaleFactor= currentWeight / 0.145;      // divide the result by a 
//                                                    // known weight

    if (scale.is_ready()) 
    {
//        float currentWeight = scale.get_units(10);    // get average of 20 
                                                      // scale readings
        long reading = 
//                -scale.read();      // varies betwee 270350+ and 270430-
//                -(scale.read() / 100);     // varies between 2703 and 2704
//                -(scale.read() / 1000);           // stable at 270
                -(scale.read() / 1000 + 270);       // stable at 0
//                -(scale.read() / 10000 + 27);     // stable at 0

        count = (count + 1) % 100;

//        lcd.clear();
//        lcd.setCursor(0, 0);        
//
//        lcd.print(count);

//        lcd.print(": ");

        if (lastReading != reading)
        {
            updated = true;
            lastReading = reading;
        }
        else if (updated)
        {
            updated = false;

            lcd.clear();
            lcd.setCursor(0, 0);        
    
            lcd.print(count);

            
            lcd.setCursor(0, 1);        // col 0, row 1

            lcd.print("weight: ");
            lcd.print(reading);
        }
    } 
    else 
        lcd.print("No HX711 found.");

//    delay(1000);
//    delay(500);
    delay(250);
  
//    lcd.setCursor(0, 0);        
//    lcd.print(scaleFactor);
//
}




