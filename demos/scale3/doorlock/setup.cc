#include "doorlock.ih"

void DoorLock::setup()
{
    d_servo.attach(s_servoPin);
    d_servo.write(90);
    delay(1000);
    d_servo.write(0);
    delay(1000);
}
