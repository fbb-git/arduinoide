#ifndef INCLUDED_DOORLOCK_
#define INCLUDED_DOORLOCK_

#include <Servo.h>

class DoorLock
{
    Servo d_servo;

    static int s_servoPin;

    public:
        DoorLock();
//        void reading(long weight, long lastWeight);

        void setup();

        void lock();
        void unlock();

    private:
//        void update();
};

#endif



