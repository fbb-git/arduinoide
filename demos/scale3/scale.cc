#include "loop.ih"

namespace
{ 
    // lock/unlock values in scale/data.cc

    Program program                         // first 2 args:  multiplication 
            {                               // and addition corrections to   
                    5000, 54,               // obtain stable 0 ratings

                    11.7188 / 1000          // to convert weight units to kgs.
            };   
}

void setup() 
{
    program.setup();
}

void loop() 
{
    program.loop();

}




