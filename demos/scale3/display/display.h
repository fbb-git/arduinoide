#ifndef INCLUDED_DISPLAY_
#define INCLUDED_DISPLAY_

#include <LiquidCrystal_I2C.h>

class Display
{
    LiquidCrystal_I2C d_lcd;

    public:
        Display();
        void setup();

        void reset();

        template<typename ...Params>
        void printRC(size_t row, size_t col, Params const &...params);

        template<typename First, typename ...Params>
        void print(First const &first, Params const &...params);

    private:
        void print();
};

template <typename ...Params>
inline void Display::printRC(size_t row, size_t col, Params const &...params)
{
    d_lcd.setCursor(col, row);
    print(params...);
}

template <typename First, typename ...Params>
inline void Display::print(First const &first, Params const &...params)
{
    d_lcd.print(first);     // doubles: by default 2 digits behind the decimal
                            // dot. 2nd argument: other number of digits
    print(params...);
}

inline void Display::print()
{}

#endif

