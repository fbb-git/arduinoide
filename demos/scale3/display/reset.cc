//#define XERR
#include "display.ih"

void Display::reset()
{
    d_lcd.clear();
    d_lcd.setCursor(0, 0);
}
