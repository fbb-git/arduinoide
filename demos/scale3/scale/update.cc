#include "scale.ih"

double Scale::update()
{
    delay(100);

    d_currentWeight = kgWeight();               // get the scale weight

    if (d_currentWeight >= 10.0)                // ignore ghost measurement
        return -1;

    if (
        d_previousWeight * 1.10 < d_currentWeight   // weight increases
        or                                          // or
        d_currentWeight < 0.9 * d_previousWeight    // weight decreases
    )
        s_stable = 0;                               // weights not stable
    else 
    {
        if (fabs(d_lastDisplayed - d_currentWeight) < 0.1)
            return -1;
                                                    // weights (almost) equal
        if (++s_stable == 1)                        // #stable measurements
        {
                    // shift all weights one position up
            for (size_t destIdx = s_nValues - 1, srcIdx = destIdx;
                    srcIdx--; --destIdx
            )
                d_value[destIdx] = d_value[srcIdx];

                // store the last measured weight
            d_value[0] = d_currentWeight;
            d_lastDisplayed = d_currentWeight;
    
            display();
        }
    }

    d_previousWeight = d_currentWeight;

    return s_stable == 1 ? d_currentWeight : -1;
}


//
//    if (d_previousWeight < d_currentWeight)         // weight increases:
//        d_up = true;                            // merely register that
//
//    else if (d_currentWeight <= d_previousWeight)   // weight stable or decreases 
//    {
//            // if going down
//        if (not d_up)
//        {
//                    // if really going down then reduce the max. weight
//            if (d_currentWeight < 0.95 * d_stableWeight)
//                d_stableWeight = d_currentWeight;
//        }
//        else        // coming from a phase where weight increased
//        {
//                    // if the increase exceeds the last registered maximum
//            if (1.05 * d_stableWeight < d_previousWeight)
//            {
//                
//                if (d_previousWeight < 0.90 * d_value[0]  ||
//                    1.10 * d_value[0] < d_previousWeight)
//                {
//                        // shift all weights one position up
//                    for (size_t destIdx = s_nValues - 1, srcIdx = destIdx;
//                            srcIdx--; --destIdx
//                    )
//                        d_value[destIdx] = d_value[srcIdx];
//    
//                        // store te last measured weight
//                    d_value[0] = d_previousWeight;
//        
//                    d_up = false;       // done going up
//        
//        
//                    display();
//                    delay(250);
//                }
//
//                    // new maximum weight
//                d_stableWeight = d_previousWeight;
//            }
//        }
//    }
//
//    d_previousWeight = d_currentWeight;
//
//    return returnWeight ? d_stableWeight : 0;
//




