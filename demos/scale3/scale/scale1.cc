#include "scale.ih"

Scale::Scale(Display &display, unsigned divide, unsigned add, 
             double weight2kg)
:
    d_display(display),

    d_previousWeight(s_unlockWeight),
//    d_stableWeight(0),

    d_divide(divide),
    d_add(add),
    d_weight2kg(weight2kg)
//    d_lockCount(0),
//    d_upCount(0),
//    d_count(0)
{
    for (size_t idx = 0; idx != s_nValues; ++idx)
        d_value[idx] = 0;
}
