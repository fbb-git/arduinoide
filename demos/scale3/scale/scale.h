#ifndef INCLUDED_SCALE_H_
#define INCLUDED_SCALE_H_

#include <HX711.h>  // Library needed to communicate with HX711
                    // https://github.com/bogde/HX711 

#include "../doorlock/doorlock.h"

class Display;

class Scale
{
    Display &d_display;
//    DoorLock d_doorLock;

    double d_lastDisplayed = 0;
    double d_currentWeight;
    double d_previousWeight;                // last measured KG weight,
                                        // initialized to d_unlockWeight

    static size_t const s_nValues = 6;
    double d_value[s_nValues];

    int const d_out = 3;
    int const d_clk = 2;

    bool d_locked = false;
    bool d_up = false;                  // weight increases

    unsigned d_divide;
    unsigned d_add;
    double d_weight2kg;
    double d_kgOffset = 0;
    HX711 d_scale;

    size_t d_count = 0;

//    size_t d_lockCount;                 // # times the door was locked
//    size_t d_upCount;
//    size_t d_displayCount = 0;

    static double s_lockWeight;
    static double s_unlockWeight;
    static size_t s_stable;

    public:
        Scale(Display &display,
              unsigned divide, unsigned add, double weight2kg);

        void setup();
        double update();

    private:
        long weight();
        double kgWeight();
        void display();                 // show statistics in d_display
};

inline long Scale::weight()
{
    return -(d_scale.read() / d_divide + d_add);
}

#endif

