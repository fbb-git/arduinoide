#include "scale.ih"

void Scale::display()
{
    d_display.reset();

    d_display.printRC(0, 0, ++d_count, ' ');

    for (size_t idx = 0, end = s_nValues / 2; idx != end; ++idx)
        d_display.print(d_value[idx], ' ');

    d_display.printRC(1, 0);
    for (size_t idx = s_nValues / 2; idx != s_nValues; ++idx)
        d_display.print(d_value[idx], ' ');

//    d_display.printRC(0, 0, d_currentWeight, 
//                            " C: ", d_count, " D: ", ++d_displayCount);
//
//    d_display.printRC(1, 0, d_previousWeight, // max
//                            " U: ", d_upCount, 
//                            " L: ", d_lockCount);
}

