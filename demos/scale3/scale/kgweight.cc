#include "scale.ih"

double Scale::kgWeight()
{
    double ret = weight()  * d_weight2kg;

    return ret < 0 ? 0 : ret;
}
