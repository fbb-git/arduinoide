//#define XERR
#include "ether.ih"

void Ether::setup()
{
    d_doorLock.setup();

    Ethernet.begin(s_mac, s_ip);

    d_display.reset();

}
