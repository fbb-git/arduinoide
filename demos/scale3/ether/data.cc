//#define XERR
#include "ether.ih"

                            // the final value(s?) may be altered
byte    Ether::s_mac[] =    { 0xde, 0xad, 0xbe, 0xef, 0xfe, 0xee };

byte    Ether::s_ip[] =     { 192, 168, 17, 16 };       // arduino

// SERVERS:

// byte    Ether::s_server[] = { 192, 168, 17, 6 };        // ballistix
byte    Ether::s_server[] = { 192, 168, 17, 17 };        // styx

size_t  Ether::s_port = 7750;


//IPAddress   Ether::s_ip     { 192, 168, 17, 16 };
//IPAddress   Ether::s_dns    { 192, 168, 17, 17 };
//IPAddress   Ether::s_gateway{ 192, 168, 17, 17 };
//IPAddress   Ether::s_subnet { 255, 255, 17, 0 };
