#ifndef INCLUDED_ETHER_
#define INCLUDED_ETHER_

#include <SPI.h>
#include <Ethernet.h>

#include "../display/display.h"
#include "../scale/scale.h"
#include "../doorlock/doorlock.h"

class Ether
{
    Display &d_display;
    Scale &d_scale;
    DoorLock d_doorLock;

//    EthernetServer d_server;            // connect to port 7750
    EthernetClient d_client;              // connect to port 7750

    static byte s_mac[];
    static byte s_ip[];
    static byte s_server[];

    static size_t s_port;

//    static IPAddress s_ip;
//    static IPAddress s_dns;
//    static IPAddress s_gateway;
//    static IPAddress s_subnet;

    public:
        Ether(Display &display, Scale &scale);

        void setup();
        void connect();

        bool client();          // true: active connection, false: reconnect

    private:
};
        
#endif
