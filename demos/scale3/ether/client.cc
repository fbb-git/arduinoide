//#define XERR
#include "ether.ih"

static size_t counter = 0;

//    d_display.reset();


bool Ether::client()
{
    if (not d_client.connected())
        return false;

    double value = d_scale.update();    // get a value from the scale

    if (value < 0)                      // nothing to do at negative values
    {
        delay(100);
        return true;                    // try again to get a weight
    }

    d_display.printRC(0, 0, "Scale value ", value);

    d_client.println(value);
//    if (d_client.println(value) <= 0)   // can't write to the server
//        return false;                   // and reconnect

    while (true)
    {
        if (not d_client.connected())
            return false;

        if (d_client.available() != 0)  // info is available
            break;                      // then process it

        delay(100);                     // wait a bit, and try again
    }
        
    switch (d_client.read())          // read the command
    {
        case 'L':
            d_doorLock.lock();
            d_display.reset();
            d_display.printRC(0, 0, ++counter, " LOCK");
            d_client.flush();
        break;

        case 'U':
            d_doorLock.unlock();
            d_display.reset();
            d_display.printRC(0, 0, ++counter, " UNLOCK");
            d_client.flush();
        break;

        default:
            d_display.reset();
            d_display.printRC(0, 0, ++counter, " no action");
            d_client.flush();
        break;
    }

    return true;                        // next round
}




