#ifndef INCLUDED_PROGRAM_
#define INCLUDED_PROGRAM_

#include "../display/display.h"
#include "../scale/scale.h"
#include "../ether/ether.h"

class Program
{
    Display d_display;
    Ether d_ethernet;
    Scale d_scale;    

    public:
        Program(unsigned divide, unsigned add, double weight2kg);

        void setup();
        void loop();

    private:
};
        
#endif
