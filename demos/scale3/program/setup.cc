#include "program.ih"

void Program::setup()
{
    d_display.setup();
    d_ethernet.setup();
    d_scale.setup();
}
