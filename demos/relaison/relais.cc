/* Blink5
 
 Turns on and off a light emitting diode(LED) connected to a digital pin. 
 After 5x second on-second off the led goes on for 5 secs, after 
 which the cycle repeats
 
 The circuit:
 * LED attached from pin 13 to ground.
 * Note: on most Arduinos, there is already an LED on the board
 that's attached to pin 13, so no hardware is needed for this example.
 
*/

#include <Arduino.h>


void setup() {
  pinMode(10, OUTPUT);      
  digitalWrite(10, LOW);

  pinMode(11, OUTPUT);      
  digitalWrite(11, LOW);
}

void loop()
{}

