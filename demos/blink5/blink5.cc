/* Blink5
 
 Turns on and off a light emitting diode(LED) connected to a digital pin. 
 After 5x second on-second off the led goes on for 5 secs, after 
 which the cycle repeats
 
 The circuit:
 * LED attached from pin 13 to ground.
 * Note: on most Arduinos, there is already an LED on the board
 that's attached to pin 13, so no hardware is needed for this example.
 
*/

    // use -I/usr/local/include/arduino, and omit the arduino/ prefix before
    // Arduino.h:
#include <Arduino.h>

int const ledPin =  13;      // the number of the LED pin

long const oneSecond = 1000;       // interval at which to blink (milliseconds)
long const fourSeconds = 4000;    // additional waiting time to make 5 secs.

int count = 1;              // counts # of iterations: immediately at startup
                            // the light is off, and we wait 1 second in the loop.
int lightCount = 0;         // counts # of times the led has been switched on

void setup() {
  // set the digital pin as output:
  pinMode(ledPin, OUTPUT);      
}

void loop()
{
    delay(oneSecond);    // always wait 1 second
    
    if (++count & 1)  // at odd counts: switch off the light and go
    {
        digitalWrite(ledPin, HIGH);
        return;
    }

    digitalWrite(ledPin, LOW);  // switch on the light
    
    if (++lightCount == 3)      // after 3 * blink: on for 5 secs.
    {
        lightCount = 0;
        delay(fourSeconds);    // wait 4 more seconds, + 1 at the top makes 5
    }
}

